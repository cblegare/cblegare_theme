const webpack = require('webpack');
const path = require('path');
const ExtractText = require('extract-text-webpack-plugin');
const Html = require('html-webpack-plugin');

const config = {
    output: {
        path: path.resolve(
            __dirname, '..','cblegare_theme', 'static'
        )
    },
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractText.extract(
                    {
                        // creates style nodes from JS strings
                        fallback: 'style-loader',
                        use: [
                            "css-loader",  // translates CSS to CommonJS
                            "sass-loader", // compiles SASS to CSS
                        ]
                    }
                )
            }, {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'images'
                        }
                    }
                ]
            }, {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts'
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new ExtractText({filename: 'style.css'}),
        new Html({
            inject: false,
            hash: true,
            template: './src/index.html',
            filename: 'index.html'
        }),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        })
    ]
};

module.exports = config;
