#######
A theme
#######

.. only:: html

    |build_badge| |cov_badge| |lic_badge| |home_badge|

    .. |build_badge| image:: https://gitlab.com/cblegare/cblegare_theme/badges/master/pipeline.svg
        :target: https://gitlab.com/cblegare/cblegare_theme/pipelines
        :alt: Build Status

    .. |cov_badge| image:: https://gitlab.com/cblegare/cblegare_theme/badges/master/coverage.svg
        :target: _static/embedded/coverage/index.html
        :alt: Coverage Report

    .. |lic_badge| image:: https://img.shields.io/badge/License-LGPL%20v3-blue.svg
        :target: http://www.gnu.org/licenses/lgpl-3.0
        :alt: GNU Lesser General Public License v3, see :ref:`license`.

    .. |home_badge| image:: https://img.shields.io/badge/gitlab-cblegare%2Fcblegare_theme-orange.svg
        :target: https://gitlab.com/cblegare/cblegare_theme
        :alt: Home Page


A sphinx theme.

Browse also the `project's home`_.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam non turpis
nec risus iaculis rutrum rutrum nec ipsum. In malesuada sem viverra mi lacinia,
quis vehicula elit aliquam. Maecenas auctor tortor a diam tincidunt dictum.
Mauris hendrerit ultrices bibendum. Curabitur suscipit luctus porttitor.
Donec finibus, turpis quis sollicitudin tempor, libero erat dictum nibh, sed
vestibulum neque est sit amet felis. Nam egestas finibus est vitae auctor.
Maecenas nunc ante, volutpat sed nulla at, ultrices tempus enim. Donec convallis
consequat sollicitudin. Mauris quam diam, bibendum eu leo at, efficitur
tristique risus. In est metus, ullamcorper eu elementum eget, feugiat ornare
quam. Sed tempus facilisis erat, vitae cursus magna blandit eget.

Vestibulum accumsan scelerisque massa. Donec id orci diam. Integer accumsan
justo a nibh congue, in iaculis justo iaculis. Curabitur sit amet euismod
tellus. Fusce eget sodales leo. Duis nibh turpis, luctus posuere hendrerit ac,
vehicula vel nulla. Phasellus a arcu ipsum. Nunc maximus vel risus tristique
condimentum. Phasellus convallis felis sit amet justo cursus condimentum eu id
dolor. Sed ac dignissim leo, at vulputate nisi.

Integer blandit, tortor in viverra sagittis, nunc nulla suscipit odio, tincidunt
convallis elit nibh ut libero. Vestibulum vel hendrerit turpis, sed malesuada
lorem. Mauris eu orci quis nisi consectetur varius. Suspendisse malesuada est
vel finibus semper. Integer eu egestas neque. Vivamus malesuada nunc at purus
porta, sed luctus mauris gravida. Praesent gravida at est vel faucibus. Donec
malesuada sem nec est luctus efficitur.

.. only:: latex

    The latest version of this document is also available online in the Docs_.

    .. _Docs: https://cblegare.gitlab.io/cblegare_theme/


.. only:: html

    The latest version of this document is also available as a |pdflink|.


.. toctree::
     :maxdepth: 2

     changelog
     license


======================
Technical Informations
======================

Here lies guides and references for contributors as well as some general
references.

.. toctree::
    :maxdepth: 2

    glossary
    release


==================
Tables and indices
==================

Quick access for not so much stuff.

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _project's home: https://gitlab.com/cblegare/cblegare_theme/
.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _RCM: http://thoughtbot.github.io/rcm/rcm.7.html

